ifndef GBDIR
$(error GBDIR (Path to Greybus directory) is mandatory)
endif

# Install directory
INSTALLDIR = /usr/bin/

CC = $(CROSS_COMPILE)gcc


CFLAGS = -Wall -g 
INCS = -I$(GBDIR) 
LFLAGS = -L$(INSTALLDIR)/lib
LIBS =  -lpthread -lusbg  -lm -lrt

SRCS = main.c gadget.c functionfs.c inotify.c manifest.c cport.c svc.c

OBJS = $(SRCS:.c=.o)


MAIN = gbsim-gw

.PHONY: depend clean install install-service

all:    $(MAIN)

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCS) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS) 

.c.o:
	$(CC) $(CFLAGS) $(INCS) -c $<  -o $@


clean:
	$(RM) *.o *~ $(MAIN)

depend: $(SRCS) 
	makedepend $(CFLAGS) $(SRCS) 
install: $(MAIN)
	install $(MAIN) $(INSTALLDIR)

install-service: install
	cp debian/init.d/gbsim /etc/init.d/gbsim
	update-rc.d gbsim defaults
	@echo ""
	@echo "GBSim configured to run at boot...  to start immediately, run:"
	@echo "    sudo service gbsim start"


# DO NOT DELETE

