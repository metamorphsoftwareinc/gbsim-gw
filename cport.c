/*
 * Greybus Simulator
 *
 * Copyright 2014 Google Inc.
 * Copyright 2014 Linaro Ltd.
 *
 * Provided under the three clause BSD license found in the LICENSE file.
 */

#include <stdlib.h>
#include <stdio.h>
#include <linux/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "gbsim.h"

#define ES1_MSG_SIZE	(4 * 1024)

/* Receive buffer for all data arriving from the AP */
static char cport_rbuf[ES1_MSG_SIZE];
static char cport_tbuf[ES1_MSG_SIZE];
static char cport_tbuf_recv[ES1_MSG_SIZE];

struct gbsim_cport *cport_find(uint16_t cport_id)
{
	struct gbsim_cport *cport;

	TAILQ_FOREACH(cport, &info.cports, cnode)
	if (cport->hd_cport_id == cport_id)
		return cport;

	return NULL;
}

void allocate_cport(uint16_t cport_id, uint16_t hd_cport_id, int protocol_id)
{
	struct gbsim_cport *cport;

	cport = malloc(sizeof(*cport));
	cport->id = cport_id;

	cport->hd_cport_id = hd_cport_id;
	cport->protocol = protocol_id;
	TAILQ_INSERT_TAIL(&info.cports, cport, cnode);
}

void free_cport(struct gbsim_cport *cport)
{
	TAILQ_REMOVE(&info.cports, cport, cnode);
	free(cport);
}

void free_cports(void)
{
	struct gbsim_cport *cport;

	/*
	 * Linux doesn't have a foreach_safe version of tailq and so the dirty
	 * trick of 'goto again'.
	 */
again:
	TAILQ_FOREACH(cport, &info.cports, cnode) {
		if (cport->hd_cport_id == GB_SVC_CPORT_ID)
			continue;

		free_cport(cport);
		goto again;
	}
	reset_hd_cport_id();
}



static int send_msg_to_ap(struct op_msg *op, uint16_t hd_cport_id,
                          uint16_t message_size, uint16_t id, uint8_t type,
                          uint8_t result)
{

	ssize_t nbytes;

	op->header.size = htole16(message_size);
	op->header.id = id;
	op->header.type = type;
	op->header.result = result;

	/* Store the cport id in the header pad bytes */
	op->header.pad[0] = hd_cport_id & 0xff;
	op->header.pad[1] = (hd_cport_id >> 8) & 0xff;



	/* Send the response to the AP */
	printf("this is being sent to ap");
	if (verbose)
		gbsim_dump(op, message_size);

	nbytes = write(to_ap, op, message_size);
	if (nbytes < 0)
		return nbytes;

	return 0;
}

int send_response(struct op_msg *op, uint16_t hd_cport_id,
                  uint16_t message_size, struct op_header *oph,
                  uint8_t result)
{

	return send_msg_to_ap(op, hd_cport_id, message_size, oph->id,
	                      oph->type | OP_RESPONSE, result);
}

int send_request(struct op_msg *op, uint16_t hd_cport_id,
                 uint16_t message_size, uint16_t id, uint8_t type)
{
	return send_msg_to_ap(op, hd_cport_id, message_size, id, type, 0);
}


void recv_thread_cleanup(void *arg)
{
	cleanup_endpoint(to_ap, "to_ap");
	cleanup_endpoint(from_ap, "from_ap");
}

/*
 * Repeatedly perform blocking reads to receive messages arriving
 * from the AP.
 */
void *recv_thread(void *param) {

	fd_set read_fd;
	FD_ZERO(&read_fd);

	FD_SET(from_ap, &read_fd);

	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	
	while (endpointCancel == 0) {
		ssize_t rsize;
		int rv;
		rv = select(from_ap + 1, &read_fd, NULL, NULL, &timeout);
		if (rv == -1)
			perror("select"); /* an error accured */
		else if (rv == 0)
			printf("timeout"); /* a timeout occured */
		else
			rsize = read(from_ap, cport_rbuf, ES1_MSG_SIZE);

		if (rsize < 0) {
			gbsim_error("error %zd receiving from AP\n", rsize);
		}

		if (verbose) {
			printf("FROM AP:");
			gbsim_dump(cport_rbuf, rsize);
		}
		struct op_header *hdr = (struct op_header*)cport_rbuf;
		struct gbsim_cport *cport;

		uint16_t hd_cport_id;

		/* Retreive the cport id stored in the header pad bytes */
		hd_cport_id = hdr->pad[1] << 8 | hdr->pad[0];
		cport = cport_find(hd_cport_id);
		if (cport) {

			if (cport->protocol == GREYBUS_PROTOCOL_SVC) 
				svc_handler(cport->id, cport->hd_cport_id, &cport_rbuf, rsize, &cport_tbuf,  sizeof(cport_tbuf));
			
		}
		else {
			int temp = 0;
			if ((temp = write(connfd, cport_rbuf, rsize)) < 0)
				printf("write error\n");
		}

		memset(cport_rbuf, 0, sizeof(cport_rbuf));
		memset(cport_tbuf, 0, sizeof(cport_tbuf));


	}
	return NULL;
}


void *tcp_rcv(void *param) {
	ssize_t recv, nbytes;
	while (1) {

		recv = 0;
		memset(cport_tbuf_recv, 0, sizeof(cport_tbuf_recv));
		recv = read(connfd, cport_tbuf_recv, ES1_MSG_SIZE);
		if (recv < 0) {
			gbsim_error("error %zd receiving from AP\n", recv);
			return NULL ;
		}
		if (recv > 0) {
			if (verbose) {
				printf("FROM SYSTEMC(TO AP):");
				gbsim_dump(cport_tbuf_recv, recv);
			}

			nbytes = write(to_ap, cport_tbuf_recv, recv);

			if (nbytes < 0)
				printf("error in writing to_ap in tcp_rcv htread\n");
		}
	}
}
