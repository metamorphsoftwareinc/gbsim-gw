/*
 * Greybus Simulator
 *
 * Copyright 2014 Google Inc.
 * Copyright 2014 Linaro Ltd.
 *
 * Provided under the three clause BSD license found in the LICENSE file.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <linux/types.h>

#include "gbsim.h"

#define MAX_NAME 256
#define INOTIFY_EVENT_SIZE  ( sizeof(struct inotify_event) )
#define INOTIFY_EVENT_BUF   ( INOTIFY_EVENT_SIZE + MAX_NAME + 1 )

static pthread_t inotify_pthread;
static pthread_t tcprecv_pthread;
int notify_fd = -ENXIO;

int listenfd = 0;
int connfd = 0;
struct sockaddr_in serv_addr;
char recvBuff[1025];
int numrv;
int bytesReceived = 0;




static int get_interface_id(char *fname)
{
	char *iid_str;
	int iid = 0;
	char tmp[256];

	strcpy(tmp, fname);
	iid_str = strtok(tmp, "-");
	if (!strncmp(iid_str, "IID", 3))
		iid = strtol(iid_str + 3, NULL, 0);

	return iid;
}

static void *inotify_thread(void *param)
{

	char filename[65] = {0};
	int ret = 0;

	if (listen(listenfd, 10) == -1) {
		printf("Failed to listen\n");
		return NULL;
	}


	connfd = accept(listenfd, (struct sockaddr*)NULL , NULL);


	if (ret < 0) {
		perror("can't create cport_tcp thread");
		return NULL;
	}
	ret = read(connfd, filename, 64);
	printf("%s\n", filename);

	if (ret) {
		int iid = get_interface_id(filename);
		if (iid > 0) {
			gbsim_info("%s Interface inserted\n", filename);
			svc_request_send(GB_SVC_TYPE_INTF_HOTPLUG, iid);
		} else
			gbsim_error("invalid interface ID, no hotplug plug event sent\n");
	} else
		gbsim_error("missing manifest blob, no hotplug event sent\n");


	ret = pthread_create(&tcprecv_pthread, NULL, tcp_rcv, NULL);
	if (ret < 0) {
		perror("can't create cport_tcp thread");
		return NULL;
	}


	return NULL;
}


//=================================for removing module=======================================
//				else if (event->mask & IN_DELETE) {
// 					int iid = get_interface_id(event->name);
//				if (iid > 0) {
//						send_hot_unplug(get_interface_id(event->name));
// 						gbsim_info("%s interface removed\n", event->name);
// 					} else
// 						gbsim_error("invalid interface ID, no hotplug unplug event sent\n");


int inotify_start()
{
	int ret;



	listenfd = socket(AF_INET, SOCK_STREAM, 0);

	printf("Socket retrieve success\n");

	memset(&serv_addr, '0', sizeof(serv_addr));
	memset(recvBuff, '0', sizeof(recvBuff));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000);

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));





	ret = pthread_create(&inotify_pthread, NULL, inotify_thread, NULL);
	if (ret < 0) {
		perror("can't create inotify thread");
		exit(EXIT_FAILURE);
	}

	return 0;
}





