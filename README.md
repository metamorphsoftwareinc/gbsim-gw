# Setting up GBSim for testing with NVidia Jetson and BeagleBone Black

## Jetson Setup

 1. Download and flash the Ara Android_For_Jetson package (latest version as of this writing is 5.1.0; package downloads should end with `lmy47z`).  Follow the install instructions on the [Ara Android wiki](https://github.com/projectara/Android-wiki/wiki/Getting-Started-with-the-Jetson-reference-platform).
      * Make sure to also install the NVidia vendor image (the process under "Instructions for flashing NVIDIA vendor fastboot image").
      * Once this is complete, the Jetson board should boot and, if connected to an HDMI display, should display the Android UI and run without errors.
 2. Rebuild the kernel and Greybus kernel modules and flash to the Jetson board by following the instructions on the [Kernel Only Build Instructions for Jetson reference platform wiki page](https://github.com/projectara/Android-wiki/wiki/Kernel-Only-Build-Instructions-for-Jetson-reference-platform).
      * At this time, our gbsim is tested against Greybus commit `2d28745`--  gbsim may not build/work when run against a newer version of Greybus.  Before building greybus, run:
      
            git checkout 2d28745
          
      * **Step 3** of the kernel build instructions is incomplete:  before creating ramdisk.gz (the second line of Step 3), you'll need to correct the permissions on the Greybus kernel modules or the Android init system won't automatically load them at boot:

            cd $JKB_ROOT/boot-files/ramdisk
            chmod 644 lib/modules/*.ko
            
      * As per the install instructions (Step 4), flash your new image to the Jetson board's boot partition:
      
            sudo fastboot flash boot newboot.img
            
 3. After flashing, reboot the Jetson board (with `sudo fastboot reboot`) and it should be ready for use with GBSim.

## BeagleBone Black Setup

These instructions assume your BeagleBone Black is running the most recent BeagleBone Black Debian release.  If it is not, update by following the instructions on the [BeagleBoard website](http://beagleboard.org/getting-started#update).

 1. If your BeagleBone is running from an SD card and you haven't done so already, expand the Linux partition to fill the SD card:

        cd /opt/scripts/tools
        sudo ./grow_partition.sh
        sudo reboot
        
 2. Run `update_kernel.sh` to add the BeagleBone kernel repositories to apt:

        cd /opt/scripts/tools
        sudo ./update_kernel.sh
        
 3. Locate the most recent 4.1 kernel release and install it (at the time of this writing, the most recent version available was 4.1.3-bone15; if a more recent version is available, install it instead):

        sudo apt-cache search linux-image | grep 4.1 | grep bone
        sudo apt-get install linux-image-4.1.3-bone15 # replace with most recent version returned by apt-cache
        sudo reboot
        
 4. Disable the BeagleBone's default mass storage and USB network gadgets:  In /opt/scripts/boot/am335x_evm.sh, comment out all modprobe lines under the comment "In a single partition setup, dont load g_multi, as we could trash the linux file system..."
 5. Reboot the Beaglebone Black:

        sudo reboot
        
### Building GBSim on BeagleBone Black

 1. Install build dependencies:

        sudo apt-get install build-essential libtool autoconf libconfig-dev
        
 2. Install build dependencies that aren't in the Debian repositories:

        git clone git@github.com:libusbg/libusbg
        pushd libusbg
        autoreconf -i
        ./configure
        make
        sudo make install
        popd
        
        git clone git@github.com:jackmitch/libsoc.git
        pushd libsoc
        ./autogen.sh
        ./configure
        make
        sudo make install
        popd
        
 3. Check out greybus (we don't need to build it):

        git clone https://github.com/projectara/greybus.git
        cd greybus
        git checkout 2d28745
        
 4. Build GBSim from this repository, install, and start GBSim in the background:

        cd gbsim-gw
        export GBDIR=$PATH_TO_GREYBUS_REPO
        make
        sudo env GBDIR=$GBDIR make install-service
        sudo service gbsim start
        
GBSim will now automatically run (and accept connections from SystemC components) when the system boots, and log its output to `/var/log/gbsim-gw.log`.  If that's not desired (i.e. for development, or to see GBSim's output in realtime), it can be stopped with `sudo service gbsim stop`.
